import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CentralModule } from './central/central.module';
import { ShopModule } from './shop/shop.module';
import { AppRouteModule } from './route/route.module';


@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, CentralModule, ShopModule, AppRouteModule],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
