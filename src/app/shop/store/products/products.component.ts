import { Component, OnInit, Input } from '@angular/core';
import { ProductModel } from '../../model/product.model';

import { CartService } from "./../cart.service";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  @Input('productItemContract') product: ProductModel;


  constructor(private cartSvc: CartService) { }

  ngOnInit() {
  }

  addToCart() {
    this.cartSvc.add(this.product);
  }


}
