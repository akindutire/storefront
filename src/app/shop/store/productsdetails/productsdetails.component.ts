import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from "@angular/router";

import { ProductsService } from "./../products.service";
import { CartService } from "./../cart.service";
import { ProductModel } from "./../../model/product.model";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-productsdetails',
  templateUrl: './productsdetails.component.html',
  styleUrls: ['./productsdetails.component.css']
})
export class ProductsdetailsComponent implements OnInit, OnDestroy {


  products: ProductModel[];

  product: ProductModel;

  productNotLoaded = true;

  productSubscription: Subscription;

  constructor(private router: Router, private activeRoute: ActivatedRoute, private productSvc: ProductsService, private cartSvc: CartService) { }

  ngOnInit() {

    this.getProduct();

    let intended_key: number;


    if (this.productNotLoaded === false) {

      this.activeRoute.params.subscribe((params: Params) => {

        intended_key = +params['itemId'];
        this.product = this.products[intended_key];

      });
    }

  }


  ngOnDestroy() {
    this.productSubscription.unsubscribe();
  }

  getProduct() {
    this.productSubscription = this.productSvc.productDetector.subscribe((product: ProductModel[]) => {
      this.products = product;
    });

    this.productSvc.productDetector.next(this.productSvc.loadProducts());

    if (this.productSvc.getProductCount() > 0) this.productNotLoaded = false;

  }

  addToCart() {

    if (this.product.itemInStock > 0) {
      this.cartSvc.add(this.product);
    } else {
      alert('Out of Stock');
    }
  }
}
