import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

import { ProductModel } from "./../model/product.model";
@Injectable({
  providedIn: 'root'
})
export class ProductsService {


  productDetector = new Subject<ProductModel[]>();

  private productCount: number = 0;

  private products: ProductModel[] = [
    new ProductModel(
      1,
      'Wear1',
      'Eye wear',
      1,
      'Smooth huggies',
      5,
      'https://source.unsplash.com/random/200x250?fashion',
      'https://source.unsplash.com/random/500x550?fashion',
      '',
      '',
      '',
      '',
      900,
      895
    ),
    new ProductModel(
      2,
      'Lace',
      'Cloth',
      1,
      'Kiss',
      2,
      'https://source.unsplash.com/random/200x251?fashion',
      'https://source.unsplash.com/random/500x551?fashion',
      '',
      '',
      '',
      '',
      950,
      925
    ),
    new ProductModel(
      3,
      'Rang',
      'Lingerie',
      2,
      'Real warmth',
      3,
      'https://source.unsplash.com/random/200x252?fashion',
      'https://source.unsplash.com/random/500x552?fashion',
      '',
      '',
      '',
      '',
      650,
      650
    ),
    new ProductModel(
      4,
      'Light Shirt',
      'Top',
      2,
      'cool',
      5,
      'https://source.unsplash.com/random/201x251?fashion',
      'https://source.unsplash.com/random/501x551?fashion',
      '',
      '',
      '',
      '',
      400,
      390
    ),
    new ProductModel(
      5,
      "Brazilian woman top 14' brown",
      'Fashion',
      3,
      'classic',
      9,
      'https://source.unsplash.com/random/200x256?fashion',
      'https://source.unsplash.com/random/500x556?fashion',
      '',
      '',
      '',
      '',
      250,
      220
    ),
    new ProductModel(
      6,
      "Indian guy 12' white",
      'Fashion',
      3,
      'attractive',
      2,
      'https://source.unsplash.com/random/202x251?fashion',
      'https://source.unsplash.com/random/502x551?fashion',
      '',
      '',
      '',
      '',
      245,
      240
    ),
    new ProductModel(
      7,
      "Bombay Jean 3'",
      'Wears',
      4,
      'well fitted',
      5,
      'https://source.unsplash.com/random/201x250?fashion',
      'https://source.unsplash.com/random/501x550?fashion',
      '',
      '',
      '',
      '',
      240,
      235
    ),
    new ProductModel(
      8,
      'X-belt',
      'Wedr',
      5,
      'audible and classic',
      18,
      'https://source.unsplash.com/random/209x250?fashion',
      'https://source.unsplash.com/random/509x550?fashion',
      '',
      '',
      '',
      '',
      300,
      298
    ),
    new ProductModel(
      9,
      "Clip braz'",
      'brax',
      6,
      'sound, visuals and color',
      9,
      'https://source.unsplash.com/random/202x258?fashion',
      'https://source.unsplash.com/random/502x508?fashion',
      '',
      '',
      '',
      '',
      340,
      335
    )
  ];

  constructor() { }

  loadProducts() {
    const products = this.products.filter((product: ProductModel) => {
      return product.itemInStock > 0;
    });

    this.productCount = products.length;
    return products;
  }

  getProductCount() {
    return this.productCount;
  }

  searchProducts(key: number) {
    let tmpProduct = null;

    // tmpProduct = this.products.find((value: ProductModel) => {
    //   value.key == key;
    // });

    tmpProduct = this.products.filter((product: ProductModel) => {
      return product.key == key;
    });

    return tmpProduct[0];
  }

  updateProduct(product: ProductModel, key: number) {
    this.products.forEach((product: ProductModel, productIndex: number) => {
      if (product.key == key) {
        this.products[productIndex] = product;
      }
    });
  }

  removeProduct(key: number) {
    let productIndex = 0;
    for (let product of this.products) {
      productIndex += 1;

      if (product.key == key) {
        this.products.splice(productIndex, 1);
        break;
      }
    }
  }


}
