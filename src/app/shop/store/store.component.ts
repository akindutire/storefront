import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Subject, Observable, Observer } from 'rxjs';

import { ProductsService } from "./products.service";
import { ProductModel } from "./../model/product.model";

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit, OnDestroy {

  products: ProductModel[];
  productNotLoaded: boolean = true;

  productSubscription: Subscription;


  constructor(private productSvc: ProductsService) { }

  ngOnInit() {
    this.getProduct();
  }

  ngOnDestroy() {
    this.productSubscription.unsubscribe();
  }

  getProduct() {
    this.productSubscription = this.productSvc.productDetector.subscribe((product: ProductModel[]) => {
      this.products = product;
    });

    this.productSvc.productDetector.next(this.productSvc.loadProducts());

    if (this.productSvc.getProductCount() > 0) this.productNotLoaded = false;

  }
}
