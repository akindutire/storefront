import { Component, OnInit, ViewChild, Renderer2, ElementRef, AfterViewInit } from '@angular/core';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, AfterViewInit {
	@ViewChild("cat_aside", { read: ElementRef }) cataside: ElementRef<any>;

	is_aside_open = false;

	category: any[] = [];
	constructor(private renderer: Renderer2) { }

	ngOnInit() {
		this.category = [{ key: 1, name: 'Shoe' }, { key: 2, name: 'Eye wear' }, { key: 3, name: 'Jeans' }, { key: 4, name: 'Caps' }, { key: 5, name: 'Pants' }, { key: 6, name: 'Lingerie' },]
	}

	ngAfterViewInit() {
		//console.log(this.cataside.nativeElement);
	}

	toggleCategory() {

		//console.log(this.cataside.nativeElement);

		if (this.is_aside_open) {
			this.renderer.setStyle(this.cataside.nativeElement, 'display', 'none');
		} else {
			this.renderer.setStyle(this.cataside.nativeElement, 'display', 'block');
		}

		this.is_aside_open = !this.is_aside_open;
	}

}
