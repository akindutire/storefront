import { NgModule } from '@angular/core';
import { CategoryComponent } from './store/category/category.component';
import { StoreComponent } from './store/store.component';
import { CartComponent } from './cart/cart.component';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './store/products/products.component';
import { ProductsdetailsComponent } from './store/productsdetails/productsdetails.component';

@NgModule({
	declarations: [CategoryComponent, StoreComponent, CartComponent, ProductsComponent, ProductsdetailsComponent],
	imports: [RouterModule, CommonModule],
	exports: []
})
export class ShopModule { }
