import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as ShopStore from './../shop/store/store.component';
import * as ShopDetails from './../shop/store/productsdetails/productsdetails.component';
import * as ShopCart from './../shop/cart/cart.component';

import { LandingComponent } from './../central/landing/landing.component';

const appRoutes: Routes = [
	{ path: '', component: LandingComponent, pathMatch: 'full' },
	{ path: 'shop', component: ShopStore.StoreComponent, pathMatch: 'full' },
	{ path: 'shop/details/:itemId', component: ShopDetails.ProductsdetailsComponent, pathMatch: 'full' },
	{ path: 'shop/cat/:catId', component: ShopCart.CartComponent, pathMatch: 'full' }
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forRoot(appRoutes)],
	exports: [RouterModule]
})
export class AppRouteModule { }
