import { NgModule } from '@angular/core';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { NavComponent } from './header/nav/nav.component';
import { CarouselComponent } from './carousel/carousel.component';
import { NewarrivalComponent } from './landing/newarrival/newarrival.component';
import { OurstoreComponent } from './landing/ourstore/ourstore.component';

@NgModule({
	declarations: [FooterComponent, HeaderComponent, LandingComponent, NavComponent, CarouselComponent, NewarrivalComponent, OurstoreComponent],
	imports: [RouterModule, CommonModule, NgbCarouselModule],
	exports: [FooterComponent, HeaderComponent]
})
export class CentralModule { }
