import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { CartService } from "./../../shop/store/cart.service";
import { Subscription } from 'rxjs';
import { ProductsService } from './../../shop/store/products.service';
import { ProductModel } from "./../../shop/model/product.model";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {

  @ViewChild("cart_list", { read: ElementRef }) cartaside: ElementRef<any>;

  is_aside_open = false;

  cartSubscription: Subscription;
  totalAmtSubscription: Subscription;

  productInCart: ProductModel[] = [];
  total: number;
  qty: number[] = [];

  isProductInCartEmpty: boolean = false;

  constructor(private cartSvc: CartService, private productSvc: ProductsService, private renderer: Renderer2) { }

  ngOnInit() {


    this.cartSubscription = this.cartSvc.itemsInCartDetector.subscribe((item: { productRef: { key: number; qty: number } }[]) => {

      this.productInCart = [];
      this.qty = [];

      for (let itm of item) {
        let pr = this.productSvc.searchProducts(itm.productRef.key);
        if (pr !== null) {

          this.productInCart.unshift(pr);
          this.qty[pr.key] = itm.productRef.qty;
        }
      }

      this.isProductInCartEmpty = this.productInCart.length == 0 ? true : false;
    });

    this.totalAmtSubscription = this.cartSvc.totalAmountDetector.subscribe(totalz => this.total = totalz);

  }

  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
    this.totalAmtSubscription.unsubscribe();
  }

  toggleCategory() {

    console.log(this.cartaside.nativeElement, this.is_aside_open);

    if (this.is_aside_open) {
      this.renderer.setStyle(this.cartaside.nativeElement, 'display', 'none');
    } else {
      this.renderer.setStyle(this.cartaside.nativeElement, 'display', 'block');
    }

    this.is_aside_open = !this.is_aside_open;
  }

}
