import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-carousel',
	templateUrl: './carousel.component.html',
	styleUrls: [ './carousel.component.css' ],
	providers: [ NgbCarouselConfig ]
})
export class CarouselComponent implements OnInit, AfterViewInit {
	images: string[] = [];
	defaultHeight = '550px';

	constructor(config: NgbCarouselConfig) {
		config.interval = 2500;
		config.wrap = true;
		config.keyboard = true;
		config.pauseOnHover = true;
	}

	ngOnInit() {
		this.images = [ './assets/img/image1.jpg', './assets/img/image2.jpg', './assets/img/image3.jpg' ];
	}
	ngAfterViewInit() {}
}
