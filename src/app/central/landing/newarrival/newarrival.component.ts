import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-newarrival',
	templateUrl: './newarrival.component.html',
	styleUrls: [ './newarrival.component.css' ]
})
export class NewarrivalComponent implements OnInit {
	arrivals: any[] = [];
	constructor() {}

	ngOnInit() {
		this.arrivals = [
			{ link: 'https://source.unsplash.com/random/400x600?fashion', price: '$900' },
			{ link: 'https://source.unsplash.com/random/400x601?fashion', price: '$800' },
			{ link: 'https://source.unsplash.com/random/400x602?fashion', price: '$400' },
			{ link: 'https://source.unsplash.com/random/400x603?fashion', price: '$540' }
		];
	}
}
